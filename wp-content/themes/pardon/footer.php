            </div><!-- #content -->
        </div><!-- .site-content-contain -->



        <footer class="site-footer desktop-footer">

            <div class="footer-container">

                <div class="f-top">
                    <div class="ft-logo">
                        <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri() ;?>/assets/images/logo.png" alt="Pardon Applications Of Canada"></a>
                    </div>
                    <div class="ft-menu-wrapper">

                        <div class="menu-container">
                            <div class="f-menu">
                                <h6>Pardons</h6>
                                <?php
                                wp_nav_menu(array(
                                    'container'      => false,
                                    'theme_location' => 'footer-pardon'
                                ));
                                ?>
                            </div>
                        </div>

                        <div class="menu-container">
                            <div class="f-menu">
                                <h6>U.S Waiver</h6>
                                <?php
                                wp_nav_menu(array(
                                    'container'      => false,
                                    'theme_location' => 'footer-uswalver'
                                ));
                                ?>
                            </div>
                        </div>

                        <div class="menu-container">
                            <div class="f-menu">
                                <h6>Record Suspension</h6>
                                <?php
                                wp_nav_menu(array(
                                    'container'      => false,
                                    'theme_location' => 'footer-record-suspension'
                                ));
                                ?>
                            </div>
                            <div class="f-menu">
                                <h6>Clear Criminal Record</h6>
                                <?php
                                wp_nav_menu(array(
                                    'container'      => false,
                                    'theme_location' => 'footer-clear-criminal-record'
                                ));
                                ?>
                            </div>
                        </div>

                        <div class="menu-container">
                            <div class="f-menu">
                                <h6>Information</h6>
                                <?php
                                wp_nav_menu(array(
                                    'container'      => false,
                                    'theme_location' => 'footer-information'
                                ));
                                ?>
                            </div>
                        </div>

                        <div class="menu-container">
                            <div class="f-menu">
                                <h6>Client Services</h6>
                                <?php
                                wp_nav_menu(array(
                                    'container'      => false,
                                    'theme_location' => 'footer-client-services'
                                ));
                                ?>
                            </div>
                        </div>

                        <div class="menu-container">
                            <div class="f-menu">
                                <h6>Follow Us</h6>
                                <?php
                                wp_nav_menu(array(
                                    'container'      => false,
                                    'theme_location' => 'footer-social'
                                ));
                                ?>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="f-bottom">
                    <div class="fb-wrapper">

                        <div class="footer-main-menu">
                            <p class="grey">Pardon Applications Of Canada &copy; <?php echo date("Y"); ?></p>
                            <?php
                            wp_nav_menu(array(
                                'container'      => false,
                                'theme_location' => 'footer-main'
                            ));
                            ?>
                        </div>
                        <div class="fb-logo">
                            <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri() ;?>/assets/images/bbb-logo.png" alt="Pardon Applications Of Canada"></a>
                        </div>
                    </div>
                </div>

            </div>

        </footer><!-- #colophon -->


        <footer id="colophon" class="site-footer tablet-footer">
            <div class="footer-container">
                <div class="tablet-footer-wrapper">
                    <div class="left-logo">
                        <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri() ;?>/assets/images/logo.png" alt="Pardon Applications Of Canada"></a>
                    </div>
                    <div class="center-menu-container">
                        <div class="f-menu">
                            <?php
                            wp_nav_menu(array(
                                'container'      => false,
                                'theme_location' => 'footer-social'
                            ));
                            ?>
                        </div>
                        <div class="footer-main-menu">
                            <p class="grey">Pardon Applications Of Canada &copy; <?php echo date("Y"); ?></p>
                            <?php
                            wp_nav_menu(array(
                                'container'      => false,
                                'theme_location' => 'footer-main'
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="right-logo">
                        <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri() ;?>/assets/images/bbb-logo.png" alt="Pardon Applications Of Canada"></a>
                    </div>
                </div>
            </div>
        </footer>


     </div><!-- #page -->



            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/loading.gif" class="loading-image hidden" alt="Dots">
 <?php wp_footer(); ?>


</body>
</html>
