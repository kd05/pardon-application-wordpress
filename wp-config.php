<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'pardon-applications' );

/** MySQL database username */
define( 'DB_USER', 'pardon-applications' );

/** MySQL database password */
define( 'DB_PASSWORD', 'geekpower1@' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '1m5$-hFVE|zU7FZ /4J #`@x Qpi.wlR_-;FbXuwz0KUR8oUJ+6+~sf}D5)N1{PI' );
define( 'SECURE_AUTH_KEY',  'xK0VV`<~.-5tlwq)Q;^3#Z4i-;PaFpD(2;Y9PIv kZ%HHcVEUbT[)De]1Ky?*5Ct' );
define( 'LOGGED_IN_KEY',    'xu-/Dx3SlcM_12jWP;|/jL,kA]HC:._^wjoXGe5oeUNcMv#eFpk4h(E_xai?e5(@' );
define( 'NONCE_KEY',        ')s8dL_jL0WIIS}O0V0M5*1cIZEtx6?i)|Dc<]_uq%O7NicdaIXwI*hWk{{<+f!x(' );
define( 'AUTH_SALT',        'x<LD(:G|rtH-9(coihzU5:`JmlH)]o2`vdF,]|<]9C>r&^8j}_|pNVdig-mWfcM8' );
define( 'SECURE_AUTH_SALT', '_fy*@jmH9x$:j3.qt^X2_z|#]rZ~f>lV6)NjK8[~JxI;NSM,z$`N1voUlZm@Ux@y' );
define( 'LOGGED_IN_SALT',   'o8)V_;uIL-4:lVd%c=qmCo`ub#=%d7O{U0FT_UF{_x]HT!^QM)pQ@f$s3.zyT0qc' );
define( 'NONCE_SALT',       '+cie29 #2@Hldm~rD=#T !i]Bi2*XWF=vVg^GA [>r`tD,!xnV9Z:xH?TwLH$B:l' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
