<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



// Register menus
register_nav_menus(
	array(
		'main-nav'          => __('The Main Menu', 'pardon'),

		'footer-pardon' => __('Footer - Pardon', 'pardon'),
		'footer-uswalver'  => __('Footer - U.S. Walver', 'pardon'),
        'footer-record-suspension' => __('Footer - Record Suspension', 'pardon'),
        'footer-clear-criminal-record' => __('Footer - Clear Criminal Record', 'pardon'),
        'footer-information' => __('Footer - Information', 'pardon'),
        'footer-client-services' => __('Footer - Client Services', 'pardon'),
        'footer-main' => __('Footer - Main', 'pardon'),
        'footer-social' => __('Footer - Social Media', 'pardon'),
	)
);


