<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}





function how_pardon_app_works_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'topTitle' => 'HOW PARDON APPLICATION WORKS',
        'mainTitle' => "We're with you every step of the way",
        'btnText' => "APPLY NOW",
        'btnLink' => "#",
    ), $atts);
    ob_start();
    $topTitle = $a['topTitle'];
    $mainTitle = $a['mainTitle'];
    $btnText = $a['btnText'];
    $btnLink = $a['btnLink'];
    ?>
    <div class="hpaw-container">
        <div class="hpaw-content">
            <h6 class="top-title"><?php  echo $topTitle; ?></h6>
            <h2><?php  echo $mainTitle; ?></h2>
            <div class="hpaw-steps-wrapper">
                <?php echo do_shortcode($content); ?>
            </div>
              <?php  echo do_shortcode('[gpButton text="'.$btnText.'" link="'.$btnLink.'"]'); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}

add_shortcode( 'howPardonAppWorks', 'how_pardon_app_works_shortcode' );





function app_step_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'icon' => 'HOW PARDON APPLICATION WORKS'
    ), $atts);
    ob_start();
    $icon = $a['icon'];
    ?>
    <div class="hpaw-step-container">
        <div class="step-inner">
            <div class="si-wrapper">
                <div class="icon-container">
                    <i class="material-icons"><?php echo $icon; ?></i>
                </div>
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}

add_shortcode( 'appStep', 'app_step_shortcode' );