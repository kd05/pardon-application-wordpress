<?php

function map_locations_cpt() {

    $labels = array(
        'name'               => _x( 'Locations', 'pardon' ),
        'singular_name'      => _x( 'Location', 'pardon' ),
        'add_new'            => _x( 'Add New Location', 'pardon' ),
        'add_new_item'       => __( 'Add New Location' ),
        'edit_item'          => __( 'Edit Location' ),
        'new_item'           => __( 'New Location' ),
        'all_items'          => __( 'All Location' ),
        'view_item'          => __( 'View Location' ),
        'search_items'       => __( 'Search Locations' ),
        'not_found'          => __( 'No Locations found' ),
        'not_found_in_trash' => __( 'No Locations found in the Trash' ),
        'menu_name'          => 'Map Locations'
    );

    $args = array(
        'labels'        => $labels,
        'description'   => '',
        'menu_position' => 11,
        'supports'      => array( 'title' ),
        'has_archive'   => true,
        'public' => true,
        'exclude_from_search' => true,
        'show_in_admin_bar'   => false,
        'show_in_nav_menus'   => false,
        'publicly_queryable'  => false,
        'query_var'           => false
    );
    register_post_type( 'maplocations', $args );

}
add_action( 'init', 'map_locations_cpt' );