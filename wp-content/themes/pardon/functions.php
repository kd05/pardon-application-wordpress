<?php
define('GOOGLE_MAP_API_KEY','AIzaSyAaxQC6sWQB9I86YMIytkn8WfQ1rWXzcY0');


// ************    Inc / Functions  ********************
require_once(get_template_directory() . '/inc/functions/enqueue-scripts.php');
require_once(get_template_directory() . '/inc/functions/menu.php');
require_once(get_template_directory() . '/inc/functions/hooks.php');
require_once(get_template_directory() . '/inc/functions/gp-functions.php');


// ************   Shortcodes     ********************
require_once(get_template_directory() . '/inc/shortcodes/twoSectionBanner.php');
require_once(get_template_directory() . '/inc/shortcodes/gpButton.php');
require_once(get_template_directory() . '/inc/shortcodes/widthContainer1360.php');
require_once(get_template_directory() . '/inc/shortcodes/widthContainer1000.php');
require_once(get_template_directory() . '/inc/shortcodes/ourServicesSlider.php');
require_once(get_template_directory() . '/inc/shortcodes/howPardonAppWorks.php');
require_once(get_template_directory() . '/inc/shortcodes/testimonialSection.php');
require_once(get_template_directory() . '/inc/shortcodes/mapPanel.php');


// ************   CPT     ********************
require_once(get_template_directory() . '/inc/customPostTypes/mapLocations.php');



// ************   Classes     ********************
require_once(get_template_directory() . '/inc/classes/gp-ajax.php');