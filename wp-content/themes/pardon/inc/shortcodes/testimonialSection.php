<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}





function testimonial_top_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
    ), $atts);
    ob_start();
    ?>
    <div class="testimonial-top-container">
        <div class="tt-content">
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}

add_shortcode( 'testimonialTop', 'testimonial_top_shortcode' );



function testimonial_section_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
    ), $atts);
    ob_start();
    ?>
    <div class="testimonial-container">
        <div class="testimonial-wrapper">
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'testimonialSection', 'testimonial_section_shortcode' );





function testimonial_single_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'name' => 'Unknown',
    ), $atts);
    ob_start();
    $name = $a['name'];
    ?>
    <div class="testimonial-single-container">
        <div class="ts-content">
            <i class="material-icons">person_outline</i>
            <?php echo do_shortcode($content); ?>
            <h6><?php echo $name; ?></h6>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'testimonialSingle', 'testimonial_single_shortcode' );