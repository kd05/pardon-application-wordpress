<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}





function our_services_slider_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'title' => 'OUR SERVICES',
    ), $atts);

    $title = $a['title'];
    ob_start();
    ?>
    <div class="services-slider-container">
        <div class="grey-layer">
            <h6><?php echo $title; ?></h6>
        </div>
        <div class="service-slider-outer-container">
            <div class="service-slider-wrapper">
                <?php  echo do_shortcode($content);   ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}

add_shortcode( 'ourServicesSlider', 'our_services_slider_shortcode' );




function service_slide_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'img' => '',
        'title' => '',
        'subtitle' => '',
        'link' => '#'
    ), $atts);

    $img_id = $a['img'];
    $img_url = ($img_id != "") ? current(wp_get_attachment_image_src($img_id, "large")) : get_image_not_found();
    $title = $a['title'];
    $subtitle = $a['subtitle'];
    $link = $a['link'];
    ob_start();
    ?>
    <div class="service-slider">
        <div class="ss-inner" style=" background-image: url('<?php echo $img_url; ?>')">
            <div class="ss-gradient"></div>
            <div class="ss-content">
                <h2><?php echo $title; ?></h2>
                <p><?php echo $subtitle; ?></p>
            </div>
            <a class="overlay-link" href="<?php echo $link; ?>"></a>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}

add_shortcode( 'servicesSlider', 'service_slide_shortcode' );

