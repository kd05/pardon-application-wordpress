<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}


function global_scripts()
{
	// Adding styles file in the header
    wp_enqueue_style('lato-font', 'https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,800,900&display=swap');
    wp_enqueue_style('montserrat-font', 'https://fonts.googleapis.com/css?family=Montserrat:400,400i,500,500i,600,600i,700,800,900&display=swap');

	wp_enqueue_style('global-styles', get_template_directory_uri() . '/assets/dist/css/style.min.css', array(), false, 'all');


    wp_enqueue_style('materialicon-styles', 'https://fonts.googleapis.com/icon?family=Material+Icons', array(), false, 'all');

    wp_enqueue_style('fontawesome-styles', get_template_directory_uri() . '/assets/dist/css/lib-css/font-awesome.min.css', array(), false, 'all');
    wp_enqueue_style( 'slick', get_template_directory_uri() . '/assets/dist/css/lib-css/slick.css' );
    wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/assets/dist/css/lib-css/slick-theme.css' );


    // Adding scripts file in the footer
    wp_register_script('moby-script', get_template_directory_uri() . '/assets/dist/js/lib-js/moby.min.js', array('jquery'), false, true );
    wp_enqueue_script( 'moby-script');

    wp_register_script('slick-rotator', get_template_directory_uri() . '/assets/dist/js/lib-js/slick.js', array('jquery'), false, true );
    wp_enqueue_script( 'slick-rotator');


    wp_register_script('google-map', 'https://maps.googleapis.com/maps/api/js?key='.GOOGLE_MAP_API_KEY, array('jquery'), false, true );
    wp_enqueue_script( 'google-map');


    wp_register_script('main-js', get_template_directory_uri() . '/assets/dist/js/main.js', array('jquery'), false, true);
    wp_enqueue_script( 'main-js');





    $js_urls = array(
        'ajax_url' => admin_url('admin-ajax.php'),
        'theme_url' => get_template_directory_uri(),
        'image_url' => get_template_directory_uri()."/assets/images/",
        'google_api_key' => GOOGLE_MAP_API_KEY,
    );
    wp_register_script('local_variables',"");
    wp_localize_script('local_variables', 'website_urls', $js_urls );
    wp_enqueue_script('local_variables');

}
add_action('wp_enqueue_scripts', 'global_scripts');
