jQuery(document).ready(function($) {


    function getMapStyle() {
        return [
            {
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#616161"
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "weight": 1.5
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "visibility": "simplified"
                    },
                    {
                        "weight": 1.5
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#bdbdbd"
                    }
                ]
            },
            {
                "featureType": "administrative.province",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#626262"
                    },
                    {
                        "weight": 1
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#eeeeee"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e5e5e5"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "weight": 1
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#dadada"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#f34b47"
                    },
                    {
                        "weight": 0.5
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#616161"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#9e9e9e"
                    }
                ]
            },
            {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e5e5e5"
                    }
                ]
            },
            {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#eeeeee"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#c9c9c9"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#9e9e9e"
                    }
                ]
            }
        ];
    }

    function initMap() {

        var zoom = 3;
        var center_lat = 51.517805;
        var center_long = -99.653463;

        if(window.innerWidth > 1368) {
            zoom = 5;
            // center_lat = 51.517805;
            // center_long = -99.653463;
        }
        else if(window.innerWidth > 1024) {
            // console.log("here 1");
            zoom = 5;
        } else if(window.innerWidth > 600){
            zoom = 4;
        }

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: zoom,
            center: {lat: center_lat, lng: center_long},
            styles : getMapStyle()
        });

        let icon = `${website_urls.image_url}map-marker.png`;

        // ******   Array for the markers on the map ************
        var locationsPos = [];
        var distancePos = [];

        $.ajax({
            url: website_urls.ajax_url,
            type: 'POST',
            async: false,
            dataType: 'JSON',
            data : {
                action : 'get_locations',
            },
            success: function(returnLocations){
                // console.log(returnLocations);

                // ************ Store the location result in the LOCAL STORAGE ************
                localStorage.removeItem('returnLocations');
                localStorage.setItem('returnLocations', JSON.stringify(returnLocations));

                returnLocations.forEach(loc => {
                    // console.log("hereee",loc['latitude'], loc['longitude']);
                    var pos = {
                        position: new google.maps.LatLng(loc['latitude'], loc['longitude']),
                    };
                    locationsPos.push(pos);
                });
            }
        });

        // console.log(locationsPos);
        // ******************************************************
        // ******   creating the marker on the map ************
        // ******************************************************
        for (var i = 0; i < locationsPos.length; i++) {
            var marker = new google.maps.Marker({
                position: locationsPos[i].position,
                icon: icon,
                map: map
            });
            // marker.addListener('click', toggleBounce);

            google.maps.event.addListener(marker, "click", function (event) {
                // marker.setAnimation(google.maps.Animation.BOUNCE);
                var marker_latitude = event.latLng.lat();
                var marker_longitude = event.latLng.lng();
                ajaxMarkerClickUpdateLocation(marker_latitude, marker_longitude);
            }); //end addListener


        };
    }

    // function toggleBounce() {
    //     if (marker.getAnimation() !== null) {
    //         marker.setAnimation(null);
    //     } else {
    //         marker.setAnimation(google.maps.Animation.BOUNCE);
    //     }
    // }






    function success(position) {
        // console.log(position);
        var latitude  = position.coords.latitude;
        var longitude = position.coords.longitude;

        getCityStateFromCoordinates(latitude,longitude);

        var origin1 = new google.maps.LatLng(latitude, longitude);
        var service = new google.maps.DistanceMatrixService();

        // ************ Retriving the location from the LOCAL STORAGE ************


        var ls_locations = JSON.parse(localStorage.getItem('returnLocations'));
        var tempLocations = ls_locations;
        // console.log("Local storage => ",tempLocations);

        // ************ create the variable to store the smllest distance id & distance (initial set to NULL) ************
        var smallestDistance = [] ;
        smallestDistance['id'] = "";
        smallestDistance['distance'] = "";
        smallestDistance['phone'] = "";
        smallestDistance['address'] = "";
        // console.log(smallestDistance);

        // ************Runnning the for each loop to check which location is nearest ************

        const displayNearestLoc = function(smallestDistance, tempLocations, callbackDisplayLocation){

            tempLocations.forEach(loct => {
                var destinationA = new google.maps.LatLng(loct['latitude'], loct['longitude']);

                service.getDistanceMatrix(
                    {
                        origins: [origin1],
                        destinations: [destinationA],
                        travelMode: 'DRIVING',
                        avoidHighways: false,
                        avoidTolls: false,
                    }, distance_callback);

                function distance_callback(response, status) {
                    // console.log(response,status);
                    if(status == "OK"){
                        // console.log(response['rows'][0]['elements'][0]['distance']['value']);
                        var locDistance = response['rows'][0]['elements'][0]['distance']['value'];

                        if(smallestDistance['distance'] == "") {
                            smallestDistance['distance'] = locDistance;
                            smallestDistance['id'] = loct['location_id'];
                            smallestDistance['phone'] = loct['phone'];
                            smallestDistance['address'] = loct['address'];
                        }
                        else{
                            if(smallestDistance['distance'] > locDistance) {
                                smallestDistance['id'] = loct['location_id'];
                                smallestDistance['phone'] = loct['phone'];
                                smallestDistance['address'] = loct['address'];
                                smallestDistance['distance'] = locDistance;
                            }
                        }
                    }
                }

            });

            callbackDisplayLocation(smallestDistance);

        }




        displayNearestLoc(smallestDistance,tempLocations,function (data) {
            setTimeout(function () {
                    $(".hpb-phone, .map-address .mp-phone").text(data['phone']);
                    $(".map-store-address").text(data['address']);
            }, 2500);

        });


    }
    function error() {
        // Doing nothinf if user doen't allow browser to locate
    }

    function getCityStateFromCoordinates(latitude,longitude){

        var geocoder;
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(latitude, longitude);

        geocoder.geocode(
            {'latLng': latlng},
            function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        var add= results[0].formatted_address ;
                        var value = add.split(",");

                        count=value.length;
                        country=value[count-1];
                        state=value[count-2];
                        city=value[count-3];

                        var showUserLoc = "";
                        if(city != "") { showUserLoc += `${city}, `;  }
                        if(state != "") { showUserLoc += `${state}`;  }
                        $( ".map-address .ma-bottom" ).prepend( `<h6>${showUserLoc}</h6>` );
                    }
                    else  {
                        // alert("address not found");
                    }
                }
                else {
                    // alert("Geocoder failed due to: " + status);
                }
            }
        );



    }


    function ajaxMarkerClickUpdateLocation(latitude,longitude){

        // alert("Here" + latitude+ longitude);
        show_loading("#map");
        $.ajax({
            url: website_urls.ajax_url,
            type: 'POST',
            dataType: 'JSON',
            data : {
                action : 'marker_update_location',
                latitude : latitude,
                longitude : longitude
            },
            success: function(returnData){
                // console.log(returnData);
                hide_loading("#map");
                if(returnData.success == 1){
                    $(".map-address .mp-phone").text(returnData.phone);
                    $(".map-store-address").text(returnData.address);

                } else {
                    alert("Sorry, No Data is available for this marker.");
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                // alert("Status: " + textStatus); alert("Error: " + errorThrown);
                alert("Something went wrong !!!");
            }

        });

    }


    if($("#map").length){

        const mapElement = document.querySelector('#map');
        initMap();

        if (!navigator.geolocation) {
            mapElement.textContent = 'Geolocation is not supported by your browser';
        } else {
            navigator.geolocation.getCurrentPosition(success, error);
        }

    }

    $(window).resize(function(e) {
        if($("#map").length){
            initMap();
        }
    });


    function show_loading(element){
        $(element).css("opacity",0.6);
        $(".loading-image").removeClass("hidden");
    }

    function hide_loading(element) {
        $(element).css("opacity", 1);
        $(".loading-image").addClass("hidden");
    }


});

