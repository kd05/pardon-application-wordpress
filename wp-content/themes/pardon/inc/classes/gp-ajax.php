<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



class GP_Ajax
{
    /**
     * GP_Ajax constructor.
     */
    public function __construct()
    {
        add_action('wp_ajax_nopriv_get_locations', array($this, 'get_locations'));
        add_action('wp_ajax_get_locations', array($this, 'get_locations'));

        add_action('wp_ajax_nopriv_marker_update_location', array($this, 'marker_update_location'));
        add_action('wp_ajax_marker_update_location', array($this, 'marker_update_location'));


    }


    public function get_locations()
    {
        $args = array(
            'post_type' => 'maplocations',
            'post_status' => 'publish',
            'posts_per_page'   => -1,
            'fields' => 'ids'
        );
        $locations = get_posts( $args );

        $position_array = [];

        if(is_array($locations) && count($locations) > 0){
            foreach ($locations as $location_id){
                $latitude = get_field("latitude",$location_id);
                $longitude = get_field("longitude",$location_id);
                $phone = get_field("phone",$location_id);
                $address = get_field("address",$location_id);
                $temp = [
                            "latitude" => $latitude,
                            "longitude" => $longitude,
                            "phone" => $phone,
                            "address" => $address,
                            "location_id" => $location_id
                        ];
                array_push($position_array, $temp);
            }
        }
        echo json_encode($position_array);
        exit;
    }






    public function marker_update_location()
    {

        $latitude = $_POST['latitude'];
        $longitude = $_POST['longitude'];

        $args = array(
            'post_type' => 'maplocations',
            'post_status' => 'publish',
            'posts_per_page'   => 1,
            'fields' => 'ids',
            'meta_query'     => [
                'relation' => 'AND',
                    [
                        'key'     => 'latitude',
                        'value'   => $latitude,
                    ],
                    [
                        'key'     => 'longitude',
                        'value'   => $longitude,
                    ]
            ]
        );

        $locations = get_posts( $args );
        if(is_array($locations) && count($locations) > 0){
            $location_id = current($locations);
            $phone = get_field("phone",$location_id);
            $address = get_field("address",$location_id);
            $return_array = [
                "success" => 1,
                "phone" => $phone,
                "address" => $address,
                "location_id" => $location_id
            ];
        } else {
            $return_array = [
                "success" => 0
            ];
        }
        echo json_encode($return_array);
        exit;
    }

}



// Initialize
global $gp_ajax;
$gp_ajax = new GP_Ajax();


