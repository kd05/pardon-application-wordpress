<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}





function two_section_banner_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
    ), $atts);
    ob_start();
    ?>
    <div class="two-section-banner-container">
        <?php echo do_shortcode($content); ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}

add_shortcode( 'twoSectionBanner', 'two_section_banner_shortcode' );






function single_banner_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'img' => '',
        'overlay' => 'red',
        'width' => ''
    ), $atts);

    $overlay = $a['overlay'] == "" ? "red" : $a['overlay'];
    $img_id = $a['img'];
    $img_url = ($img_id != "") ? current(wp_get_attachment_image_src($img_id, "large")) : get_image_not_found();
    ob_start();
    ?>
    <div class="single-banner-panel"  style=" background-image: url('<?php echo $img_url; ?>')">
        <div class="b-gradient <?php echo $overlay; ?>"></div>
        <div class="sbp-wrapper">
            <div class="sbp-content <?php  echo $a['width']; ?>">
                <?php  echo do_shortcode($content); ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}

add_shortcode( 'singleBanner', 'single_banner_shortcode' );


