<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}





function width_container_1360_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'background' => '#f1efee',
        'width' => ''
    ), $atts);
    ob_start();
    ?>
    <div class="width-container-1360" style="background-color: <?php echo $a['background']; ?>">
        <div class="wc1360-content">
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}

add_shortcode( 'widthContainer1360', 'width_container_1360_shortcode' );


