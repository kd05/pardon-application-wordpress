<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />

	<?php wp_head(); ?>
</head>


<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">

    <div class="header-fixed-background"></div>
    <header id="masthead" class="site-header" role="banner">
        <div class="header-wrap">

            <div class="top-left">
                <div class="logo">
                    <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri() ;?>/assets/images/logo.png" alt="Pardon Applications Of Canada"></a>
                </div>

                <nav class="header-menu">
                    <?php
                    wp_nav_menu(array(
                        'container'      => false,
                        'theme_location' => 'main-nav'
                    ));
                    ?>
                </nav>
            </div>

            <div class="top-right">




                <div class="header-phone">
                    <p class="hp-top">Live Officers Available - Call Now!</p>
                    <p class="hp-bottom">
                        <span class="hpb-icon">
<!--                            <i class="fa fa-phone"></i>-->
                            <i class="material-icons">phone</i>
                        </span>
                        <?php $defLoc = get_default_map_location();?>
                        <span class="hpb-phone"><?php echo $defLoc['phone']; ?></span>
                    </p>
                </div>

                <div class="navbars" id="moby-button">
                    <i class="fa fa-bars"></i>
                </div>

            </div>

        </div>
        <!--  header-wrap-->
    </header><!-- #masthead -->


    <div class="site-content-contain">
        <div id="content" class="site-content">