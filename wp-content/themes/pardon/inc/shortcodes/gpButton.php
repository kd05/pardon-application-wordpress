<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function gp_button_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'text' => '',
        'link' => '',
        'color' => 'black',
    ), $atts);
    ob_start();
    ?>
    <div class="btn-wrapper" >
        <a href="<?php echo $a['link']; ?>" class="btn <?php echo $a['color']; ?>"><?php echo $a['text']; ?></a>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'gpButton', 'gp_button_shortcode' );




function gp_link_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'text' => '',
        'link' => '',
        'color' => '#ed2227',
    ), $atts);
    ob_start();
    ?>
    <div class="link-wrapper" >
        <a href="<?php echo $a['link']; ?>" class="" style="color: <?php echo $a['color']; ?>;"><?php echo $a['text']; ?></a>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'gpLink', 'gp_link_shortcode' );