<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}





/**
 * Generates an excerpt of the desired text.
 * If no second paramter is passed then it will generate an excerpt 20 words long.
 * If any words are cut off by the excerpt then ( ... ) will be appended to the text.
 * Returns a string.
 *
 * @param string $content text that you would like an excerpt of
 * @param int $num number of words to contain in excerpt
 *
 * @return string
 */
function gp_excerptize($content, $num = 30)
{
    $number = $num;
    //$content = apply_filters('the_content', $content);
    //echo "<pre style='display: none;'>".print_r($content, true)."</pre>";
    // $content = strip_tags( $content, '<br>' );
    $content = strip_tags($content, '');
    $content = preg_replace("~(?:\[/?)[^/\]]+/?\]~s", '', $content);
    $content = str_replace('&nbsp;', '', $content);

    $contentArray = explode(' ', $content, $number + 1);
    //echo '<pre>'.print_r($contentArray, true).'</pre>';
    $contentString = '';
    foreach ($contentArray as $key => $value) {
        if ($key >= $number) {
            $contentString .= '...';
            break;
        }
        $contentString .= trim($value);
        if ($key < $number - 1) {
            $contentString .= ' ';
        }
    }

    return $contentString;
}



function get_default_map_location(){

    $args = array(
        'meta_query'        => array(
            array(
                'key'       => 'default_address',
                'value'     => 'yes'
            )
        ),
        'post_type'         => 'maplocations',
        'posts_per_page'    => '1',
        'fields' => 'ids'
    );
    $location = get_posts( $args );
    if(is_array($location) && count($location) > 0){
        $location_id = current($location);
        $latitude = get_field("latitude",$location_id);
        $longitude = get_field("longitude",$location_id);
        $phone = get_field("phone",$location_id);
        $address = get_field("address",$location_id);
        return [
                "latitude" => $latitude,
                "longitude" => $longitude,
                "phone" => $phone,
                "address" => $address
        ];
    } else{
        return [
            "latitude" => "",
            "longitude" => "",
            "phone" => "",
            "address" => ""
        ];
    }
}


function get_image_not_found()
{
    return get_template_directory_uri() . "/assets/images/not-found.png";
}


function get_default_banner(){
    return get_template_directory_uri()."/assets/images/home-banner.jpg";
}

