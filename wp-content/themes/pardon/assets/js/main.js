jQuery(document).ready(function($) {


    // **************************************************
    // **********  Moby Menu   *******
    // **************************************************
    var mobyMenu = new Moby({
        menu        : jQuery('.top-left ul.menu'), // The menu that will be cloned
        mobyTrigger : jQuery('#moby-button'), // Button that will trigger the Moby menu to open
        subMenuOpenIcon  : '<i class="material-icons">&#xE313;</i>',
        subMenuCloseIcon : '<i class="material-icons">&#xE316;</i>',
    });



    // **************************************************
    // **********    Services Slide (Slick) **********
    // **************************************************
    if($(".service-slider-wrapper").length){

        $('.service-slider-wrapper p').each(function() {
            var $this = $(this);
            if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
                $this.remove();
        });

        // Init Service Slide
        $('.service-slider-wrapper')
            .slick({
                dots: false,
                infinite: false,
                arrows: false,
                slidesToShow: 2,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 1,
                        }
                    }
                ]
        });

    }







    // **************************************************
    // **********    How App Works (Slick) **********
    // **************************************************
    const how_app_device_width = 840;
    function howAppWorks() {

        if($(".hpaw-steps-wrapper").length){

            $('.hpaw-steps-wrapper p').each(function() {
                var $this = $(this);
                if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
                    $this.remove();
            });

            $(".hpaw-steps-wrapper").slick({
                dots: true,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 960,
                        settings: {
                            slidesToShow: 1,
                        }
                    },
                ]
            });
        }

    }

    // console.log(window.innerWidth,how_app_device_width);
    if(window.innerWidth < how_app_device_width) {
        // console.log("here");
        howAppWorks();
    }

    $(window).resize(function(e){
        if(window.innerWidth < how_app_device_width) {
            setTimeout(function () {
                if(!$('.hpaw-steps-wrapper').hasClass('slick-initialized')){
                    howAppWorks();
                }
            },100);


        }else{
            setTimeout(function () {
                if($('.hpaw-steps-wrapper').hasClass('slick-initialized')){
                    $('.hpaw-steps-wrapper').slick('unslick');
                }
            },100);
        }
    });





    // **************************************************
    // **********    How App Works (Slick) **********
    // **************************************************
    const testimonial_width = 700;
    function testimonialSlick() {

        if($(".testimonial-wrapper").length){

            $('.testimonial-wrapper p').each(function() {
                var $this = $(this);
                if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
                    $this.remove();
            });

            $(".testimonial-wrapper").slick({
                dots: true,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 700,
                        settings: {
                            slidesToShow: 1,
                        }
                    },
                ]
            });
        }

    }

    // console.log(window.innerWidth,how_app_device_width);
    if(window.innerWidth < testimonial_width) {
        // console.log("here");
        testimonialSlick();
    }

    $(window).resize(function(e){
        if(window.innerWidth < testimonial_width) {
            setTimeout(function () {
                if(!$('.testimonial-wrapper').hasClass('slick-initialized')){
                    testimonialSlick();
                }
            },100);


        }else{
            setTimeout(function () {
                if($('.testimonial-wrapper').hasClass('slick-initialized')){
                    $('.testimonial-wrapper').slick('unslick');
                }
            },100);
        }
    });


});

