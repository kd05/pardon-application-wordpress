<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function map_panel_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
    ), $atts);
    ob_start();
    $location = get_default_map_location();
    ?>
    <div class="map-container">
        <div class="map-location" id="map"></div>
        <div class="map-address">
            <div class="ma-top">
                <p>Nearest Location to You:</p>
            </div>
            <div class="ma-bottom">
                <p class="map-store-address"><?php echo $location['address'] ?></p>
                <p class="map-phone">
                    <span><i class="material-icons">phone</i></span>
                    <span class="mp-phone"><?php echo $location['phone'] ?></span>
                </p>
                <span class="map-info-note">*Appointment Required</span>
            </div>

        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'mapPanel', 'map_panel_shortcode' );